from SSCon import SSCon_run
import torch
import pandas as pd
from xgboost import XGBClassifier
from xgboost_classifier import print_model_performance
import numpy as np


def main():
    # Your data preprocessing code goes here...

    # Define the input arguments for SSCon_run
    device = torch.device("cpu")  # Device to run the function on
    unlabeled_train_df = pd.read_pickle("data/unlabeled_training_data.pkl")  # DataFrame of unlabeled training data
    labeled_train_df = pd.read_pickle("data/labeled_training_data.pkl")  # DataFrame of labeled training data
    val_df = pd.read_pickle("data/validation_data.pkl")  # DataFrame of validation data
    test_df = pd.read_pickle("data/test_data.pkl") # Dataframe for test data
    featureCol = "combined_embed"  # Name of the feature column in the DataFrames
    labelCol = 'act'
    labelList = labeled_train_df[labelCol].unique().tolist()


    results = SSCon_run(
    device,
    unlabeled_train_df,
    labeled_train_df,
    val_df,
    featureCol = featureCol,
    labelList = labelList,
    labelCol = labelCol,
    HL_FEATURE_DIM = 1408,
    prob_thrld = 85,
    biencoder_path = "trained_models",
    biencoder_epoch = 5,
    iteration_cnt = 5

    )




    # Print Test Data Results 
    params = {
                'objective':'multi:softprob',
                'n_estimators':500,
                'num_class':len(labelList),
                "tree_method": 'gpu_hist'
            }         
                
    # instantiate the classifier 
    xgb_clf = XGBClassifier(**params)
    xgb_clf.load_model("trained_models/xgboost_classifier.json")

    bimodel = torch.load("trained_models/bimodel.pt").to(device)
    bimodel.eval()

    tst = torch.tensor(np.vstack(test_df[featureCol].values)).to(device)
    X_test = np.vstack(list(bimodel(tst).cpu().detach().numpy()))

    print("test_stats")
    res = print_model_performance(test_df[labelCol].values,
                                xgb_clf.predict(X_test))



if __name__ == "__main__":
    main()
