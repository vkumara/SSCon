import torch
import time

# Define the size of the tensors
size = (20000, 20000)

# Create tensors on CPU
tensor_cpu = torch.randn(size)

# Create tensors on GPU if available
device = torch.device("mps")
tensor_gpu = torch.randn(size, device=device)

# Perform a simple computation on CPU
start_time_cpu = time.time()
result_cpu = torch.matmul(tensor_cpu, tensor_cpu)
end_time_cpu = time.time()

# Perform the same computation on GPU
start_time_gpu = time.time()
result_gpu = torch.matmul(tensor_gpu, tensor_gpu)
end_time_gpu = time.time()

# Calculate the elapsed time for CPU and GPU computations
elapsed_time_cpu = end_time_cpu - start_time_cpu
elapsed_time_gpu = end_time_gpu - start_time_gpu

# Print the elapsed time for CPU and GPU computations
print(f"Elapsed time (CPU): {elapsed_time_cpu:.6f} seconds")
print(f"Elapsed time (GPU): {elapsed_time_gpu:.6f} seconds")
print(f"GPU speedup over CPU: {elapsed_time_cpu / elapsed_time_gpu:.2f}x")
